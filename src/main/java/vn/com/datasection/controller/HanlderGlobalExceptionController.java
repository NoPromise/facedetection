package vn.com.datasection.controller;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;;
@ControllerAdvice
public class HanlderGlobalExceptionController {
@ResponseStatus(HttpStatus.NOT_FOUND)
@ExceptionHandler(NoClassDefFoundError.class)
@ResponseBody
public String noHandlerException(){
	return "Không tìm thấy kết ";
}
@ResponseStatus(HttpStatus.SERVICE_UNAVAILABLE)
@ExceptionHandler(RuntimeException.class)
@ResponseBody
public String serviceUnvailable(){
	return "Server bị gián đoạn. Vui lòng bấm (F5) và thực hiện lại";
}
}
