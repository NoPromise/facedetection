package vn.com.datasection.controller;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import javax.imageio.IIOException;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import vn.com.datasection.bean.ImageP;

@Controller
@RequestMapping("/")
public class HomeController {
	Logger logger = Logger.getLogger(HomeController.class);
	@RequestMapping("/")
	public String HomePage(){
		return "uploadImage";
	}
	@RequestMapping(value = "/imageProcess", method = RequestMethod.POST,produces ="application/json" )
	@ResponseBody
	public String handleImage(@RequestParam(value="imgP",required = false) MultipartFile file,HttpServletResponse response) throws Exception{
		File tempImg = null;
		String url = "";
		if(!file.isEmpty()){
			try {
				byte[] bytes = file.getBytes();
				String nameFile = file.getOriginalFilename();
				logger.info("File that User uploaded : " + nameFile);
				String rootPathServer = System.getProperty("catalina.home");
				File folderTemp = new File(rootPathServer + File.separator + "tempImageFile");
				if (!folderTemp.exists()) {
					folderTemp.mkdir();
				}
				tempImg = new File(folderTemp.getAbsolutePath() + File.separator + nameFile);
				BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(tempImg));
				stream.write(bytes);
				stream.close();
				//String filePath = tempImg.getAbsolutePath();
				String link = "http://public.datasection.com.vn:7208/v1/facedet";
				HttpClient client = HttpClientBuilder.create().build();
				HttpPost post = new HttpPost(link);
				MultipartEntityBuilder builder = MultipartEntityBuilder.create();
				builder.addBinaryBody("image", tempImg,ContentType.APPLICATION_OCTET_STREAM,"image.ext");
				HttpEntity entity = builder.build();
				post.setEntity(entity);
				HttpResponse res = client.execute(post);
				InputStream is = res.getEntity().getContent();
				BufferedReader reader = new BufferedReader(new InputStreamReader(is));
				StringBuffer results = new StringBuffer();
				String line = "";
				while((line = reader.readLine())!=null){
					results.append(line);
				}
				logger.info("Rest result :"+results);
				//String command = "curl --form image=@"+filePath+" http://public.datasection.com.vn:7208/v1/facedet";
//				String command = "curl --form image=@"+filePath+" "+link;
//				Process process = Runtime.getRuntime().exec(command);
//				process.waitFor();
//				String results = IOUtils.toString(process.getInputStream());
//				
//				logger.info("Results :"+results);
				if(results.equals("")){
					url ="NoResults";
				}else{
				JSONObject ob = new JSONObject(results.toString());
				JSONArray arr = ob.getJSONArray("results");
				if(arr.length() == 0){
					url="NoResults";
				}else{
				List<ImageP> lstIMG = new ArrayList<ImageP>();
				for (int i = 0; i < arr.length(); i++) {
					JSONObject obj = arr.getJSONObject(i);
					String group = obj.getString("group");
					JSONArray arrRect = obj.getJSONArray("rectangle");
					JSONArray arr1 = arrRect.getJSONArray(0);
					JSONArray arr2 = arrRect.getJSONArray(1);
					int rectX = Integer.parseInt(arr1.get(0).toString());
					int rectY = Integer.parseInt(arr1.get(1).toString());
					int width = Integer.parseInt(arr2.get(0).toString());
					int height = Integer.parseInt(arr2.get(1).toString());
					ImageP img = new ImageP();
					img.setGroup(group);
					img.setCoordinateX(rectX);
					img.setCoordinateY(rectY);
					img.setWidth(width);
					img.setHeight(height);
					lstIMG.add(img);
				}
				processImage(tempImg, lstIMG);
				BufferedImage bimage;
				try{
					bimage= ImageIO.read(tempImg);
					ByteArrayOutputStream bos = new ByteArrayOutputStream();
					String ext = tempImg.getName().split("\\.")[1];
					ImageIO.write(bimage, ext, bos);
					bos.flush();
					byte[] img = bos.toByteArray();
					url = "data:image/"+ext+";base64,"+Base64.getEncoder().encodeToString(img);
					tempImg.delete();
					bos.close();
				}catch(IIOException e){
					logger.error("Lỗi đọc file ảnh sau xử lý ",e);
					url = "NoResults";
				}
			    }
				}
			} catch (IOException e) {
				logger.error("Error convert file to bytes[]",e);
			}	
		}
		JSONObject link = new JSONObject();
		link.append("linkIMG", url);
		return link.toString();
	}
	private void processImage(File file,List<ImageP> lstImg){

		try {
			/* Read image file into BufferedImage */
			BufferedImage bimage = ImageIO.read(file);
			/* Create Graphic from bufferedImage */
			Graphics2D graphic = bimage.createGraphics();
			graphic.setStroke(new BasicStroke(3.0f));
			graphic.setFont(new Font("Serif",Font.BOLD,12));
			for (ImageP img : lstImg) {
				setColorByGroup(graphic, img.getGroup());
				int width = img.getWidth() - img.getCoordinateX()-20;
				int height = img.getHeight() - img.getCoordinateY()-15;
				graphic.drawRect(img.getCoordinateX()+15, img.getCoordinateY(),width, height);
				graphic.drawString(img.getGroup(), img.getCoordinateX()+10, img.getCoordinateY()+height+30);
			}
			/* Export graphic to file */
			String extendFile = file.getName();
			String[] str = extendFile.split("\\.");
			ImageIO.write(bimage, str[1], file);
		} catch (IOException e) {
			logger.info("Error write image file");
		}
	}
	private void setColorByGroup(Graphics2D graphic,String group){
		graphic.setColor(Color.ORANGE);
		if(group.equalsIgnoreCase("ADULT_MAN")){
			graphic.setColor(Color.RED);
		}else
			if(group.equalsIgnoreCase("ADULT_WOMAN")){
			graphic.setColor(Color.GREEN);
		}else
			if(group.equalsIgnoreCase("CHILD_BOY")){
				graphic.setColor(Color.WHITE);
			}else
				if(group.equalsIgnoreCase("CHILD_GIRL")){
					graphic.setColor(Color.YELLOW);
				}
	}
}
