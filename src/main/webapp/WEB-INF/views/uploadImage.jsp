<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Latest compiled and minified CSS -->
<link rel="shortcut icon" href="http://datasection.com.vn/wp-content/uploads/2015/09/favicon.ico" type="image/x-icon">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link rel="stylesheet" href="css/facedetection.css"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script src = "https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<title>DataSection Việt Nam </title>
</head>
<body>
<div class="container" ng-app="faceDetection" ng-controller ="processImg" style ="margin-top:20px">
  <div class="row col-md-12">
    <div class="col-md-12">
      <div class="panel panel-primary" >
        <div class="panel panel-heading"><h3>Face Detection</h3><h5><a target="_blank" href="http://datasection.com.vn/" class="lnkSource">@Datasection Việt Nam</a></h5></div>
        <div class="panel panel-body">
            <div class="row">
              <div class="col-md-8">
                  <div class="row">
                    <div class="col-md-3">
                      <span class="btn btn-success btn-lg btn-file">Chọn ảnh <input type="file" ng-file="getFile($file)" id="imgFile"/></span>
                    </div>
                    <div class="col-md-5">
                      <span class="nameIMG" id="nameF"><h4>{{nameFile}}</h4></span>
                    </div>
                  </div>
               </div>
               <div class="col-md-4">
                 <input type="button" class="btn btn-warning btn-lg" id="upload" value="Upload" ng-click="uploadFile()"/>
               </div>
            </div>
            <div class="row col-md-12">
              <hr/>
            </div>
            <div class="row">
              <div class="col-md-6">
                <img class="img-responsive img-rounded"  id="imgPreview" ng-src="{{preview}}">
              </div>
              <div class="col-md-6">
                <div class="showIMG">
                  <img class="img-responsive img-rounded"  data-toggle="modal" data-target="#myModal" id="imgResponse" ng-src="{{imgRes}}">
                </div>
              </div>
            </div>
         </div>
       </div>
    </div>
  </div>
  <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" style="width:90%">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <img id="imgLarge" width="100%" max-height="600px" ng-src="{{imgRes}}" class="img-rounded"/>
    </div>
  </div>
  <div id="overBG" class="overBG">
   <div id='spinner' class='spinner'></div>
 </div>
</div>
<script src="js/spin.js"></script>
<script src="js/image.js"></script>
</body>
<footer>
  Copyright@Datasection.Vietnam
</footer>
</html>